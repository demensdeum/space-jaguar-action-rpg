function GameMainMenuController() {

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      addText(GLOBAL_APP_TITLE);
      addButton("New Game", "newGame");
      addButton("Exit", "exit");
      this.initialized = true;
    }
  };

  this.startNewGame = function() {
    addButton("WWWWWWWWA", "wut");
  };

  this.step = function() {
    this.initializeIfNeeded();
    if (isButtonPressed("newGame")) {
      this.startNewGame();
    }
    else if (isButtonPressed("exit")) {
      print("Bye-Bye Space Maggot!");
      exit(0);
    }
  };

}
