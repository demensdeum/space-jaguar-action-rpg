function switchToController(controller) {
  GLOBAL_Context.switchToController(controller);
}

if (GLOBAL_Context_Initialized != true) {
  include("com.demensdeum.spacejaguaractionrpg.includes.js");
  IncludeDependencies();

  GLOBAL_APP_TITLE = "Space Jaguar Action RPG v1.0.0";
  addDefaultCamera();
  var controller = new GameMainMenuController();
  GLOBAL_Context = new Context();
  switchToController(controller);
  GLOBAL_Context_Initialized = true;
}

GLOBAL_Context.step();
